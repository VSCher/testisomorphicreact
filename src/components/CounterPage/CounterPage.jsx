import React, { Component } from 'react';
// import PageHeader from 'react-bootstrap/lib/PageHeader';
import StateCounter from './StateCounter';
import ReduxCounter from './ReduxCounter';

class CounterPage extends Component {
  render() {
    return (<div>
      <StateCounter/>
      <ReduxCounter />
    </div>);
  }
}

export default CounterPage;
