import React, { Component, PropTypes } from 'react';
import Counter from './Counter';

const propTypes = {
  onClick: PropTypes.func,
  value: PropTypes.number
};

class StateCounter extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
    this.state = { value: 0 };
  }

  handleClick() {
    this.setState({ value: this.state.value + 1 });
  }

  render() {
    return (<Counter value={this.state.value} onClick={this.handleClick}/>);
  }
}

StateCounter.propTypes = propTypes;
export default StateCounter;
