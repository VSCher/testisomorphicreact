import React, { PropTypes, Component } from 'react';
import './MainPage.css';

const propTypes = {
  initName: PropTypes.string
};

const defaultProps = {
  initName: 'Анон'
};

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChangeName = this.handleChangeName.bind(this);
    this.renderGreetingWidget = this.renderGreetingWidget.bind(this);
    this.state = {
      name: this.props.initName,
      touched: false,
      greetingWidget: () => null
    };
  }

  handleChangeName(value) {
    const name = value.target.value;

    this.setState({ touched: true });
    if (name.length === 0) {
      this.setState({ name: this.props.initName });
    } else {
      this.setState({ name });
    }
  }

  renderGreetingWidget() {
    if (!this.state.touched) {
      return (<div>Привет {this.state.name}</div>);
    }

    return (
      <div>{this.state.name}</div>
    );
  }

  render() {
    return (
      <div className='App'>
        <div><input onChange={this.handleChangeName}/></div>
        <div>{this.renderGreetingWidget()}</div>
      </div>
    );
  }
}

App.propTypes = propTypes;
App.defaultProps = defaultProps;

export default App;
