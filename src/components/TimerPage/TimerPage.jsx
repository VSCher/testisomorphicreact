import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import PageHeader from 'react-bootstrap/lib/PageHeader';
import Button from 'react-bootstrap-button-loader';
import { timeRequest } from 'redux/actions/timeActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  time: PropTypes.number
};

class TimerPage extends Component {
  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.dispatch(timeRequest());
  }

  render() {
    const { loading, time } = this.props;

    return (
      <div>
        <PageHeader>Hello TimerPage</PageHeader>
        <Button loading={loading} onClick={this.handleClick}>Запросить</Button>
        {time && <div>Time: {time}</div>}
      </div>
    );
  }
}

TimerPage.propTypes = propTypes;

function mapStateToProps(state) {
  const { time, loading } = state.time;
  return { time, loading };
}
export default connect(mapStateToProps)(TimerPage);
