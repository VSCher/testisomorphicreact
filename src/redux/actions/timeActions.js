export const TIME_REQUEST_STARTED = 'TIME_REQUEST_STARTED';
export const TIME_REQUEST_FINISHED = 'TIME_REQUEST_FINISHED';
export const TIME_REQUEST_ERROR = 'TIME_REQUEST_ERROR';

function timeRequestStarted() {
  return {
    type: TIME_REQUEST_STARTED
  };
}

function timeRequestFinished(time) {
  return {
    type: TIME_REQUEST_FINISHED,
    time
  };
}

function timeRequestError(error) {
  return {
    type: TIME_REQUEST_STARTED,
    error
  };
}

export function timeRequest() {
  return (dispatch) => {
    dispatch(timeRequestStarted());
    return setTimeout(() => {
      return dispatch(timeRequestFinished(Date.now()));
    }, 1000);
  };
}
