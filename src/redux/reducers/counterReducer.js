import { COUNTER_INCREMENT } from 'redux/actions/counterActions';

const initialState = { value: 0 };

export default function (state = initialState, action) {
  switch (action.type) {
    case COUNTER_INCREMENT :
      return { value: state.value + 1 };
    default :
      return state;
  }
}
