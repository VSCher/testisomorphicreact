import { combineReducers } from 'redux';
import counterReducer from './counterReducer';
import timeRuducer from './timerReducer';

export default combineReducers({
  counter: counterReducer,
  time: timeRuducer
});
