import { TIME_REQUEST_STARTED, TIME_REQUEST_FINISHED, TIME_REQUEST_ERROR } from '../actions/timeActions';

const initalState = {
  time: null,
  errors: null,
  loading: false
};

export default function (state = initalState, action) {
  switch (action.type) {

    case TIME_REQUEST_STARTED :
      return Object.assign({}, state, { loading: true, errors: null });

    case TIME_REQUEST_FINISHED :
      return Object.assign({}, state, { time: action.time, loading: false, errors: null });

    case TIME_REQUEST_ERROR :
      return Object.assign({}, state, { loading: false, error: action.errors });

    default:
      return state;
  }
}
