import React from 'react';
import { IndexRoute, Route } from 'react-router';
import App from 'components/App';
import CounterPage from 'components/CounterPage';
import MainPage from 'components/MainPage';
import TimerPage from 'components/TimerPage';

export default (
  <Route component={App} path='/'>
    <IndexRoute component={MainPage}/>
    <Route component={CounterPage} path='counters'/>
    <Route component={TimerPage} path='time'/>
    <Route component={CounterPage} path='public'/>
  </Route>
);
